var app = angular.module("sigaMobileApp");

app.controller("FactoryController",
function($scope, Alunos) {
  $scope.data = { aa_letivo: 2014, cd_unidade: 1, sq_turma: 2014000100033938 }

  $scope.filter = function() {
    $scope.alunos = Alunos.alunosPorTurma($scope.data);
  };

  $scope.action = function(aluno) {
    aluno.info = "Hello!"
    aluno.$delete();
  };
});

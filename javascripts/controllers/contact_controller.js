var app = angular.module("sigaMobileApp");

app.controller("ContactController",
function($scope, $location) {
  $scope.departments = [{value: "1", label: "Financeiro"},
                        {value: "2", label: "RH"}];

  $scope.submit = function() {
    $scope.successMessage = true;
  }
});

var app = angular.module("sigaMobileApp", ["ngRoute", "ngResource"]);

app.config(function($routeProvider) {
    $routeProvider
     .when('/', {
      templateUrl: '/cinbesa2/main.html',
      controller: 'MainController'
    }).when('/login', {
      templateUrl: '/cinbesa2/login.html',
      controller: 'LoginController'
    }).when('/contact', {
      templateUrl: '/cinbesa2/contact.html',
      controller: 'ContactController'
    }).when('/factory', {
      templateUrl: '/cinbesa2/factory.html',
      controller: 'FactoryController'
    }).otherwise({
        redirectTo: '/'
    });
  });

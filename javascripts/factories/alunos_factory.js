var app = angular.module("sigaMobileApp");

app.factory("Alunos", function($resource) {
  var resource = $resource("http://homologa.cinbesa.com.br/wssmc/api/aluno/get/:id", { id: "@idAluno" }, {
    alunosPorTurma: { url: "http://homologa.cinbesa.com.br/wssmc/api/aluno/get/alunosPorTurma", method: "GET", isArray: true }
  })

  return resource;
});
